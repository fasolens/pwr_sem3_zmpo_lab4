//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_LAB4_BOAT_HPP
#define PWR_SEM3_ZMPO_LAB4_BOAT_HPP


#include <iostream>
#include "Vehicle.hpp"

class Boat : virtual public Vehicle{
protected:
    void go_by_water (int i) {
        odometer += trip[i].get_dst_by_wtr();
    }

public:
    Boat() {
        std::cout << "Boat default constructor" << std::endl;
    }
    Boat(char const* brand) : Vehicle(brand) {
        std::cout << "Boat brand constructor" << std::endl;
    }
    void go() {
        for (int i = 0; i < distance; ++i) {
            go_by_water(i);
        }
    }

};


#endif //PWR_SEM3_ZMPO_LAB4_BOAT_HPP
