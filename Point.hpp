//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_LAB4_POINT_HPP
#define PWR_SEM3_ZMPO_LAB4_POINT_HPP


#include <cmath>

class Point {
protected:
    float distance_by_water;
    float distance_by_ground;
public:
    Point() {
        distance_by_ground = 0;
        distance_by_water = 0;
    }
    Point(float wtr, float gnd) {
        distance_by_water = wtr;
        distance_by_ground = gnd;
    }
    float get_dst_by_wtr() {
        return distance_by_water;
    }
    float get_dst_by_gnd() {
        return distance_by_ground;
    }
};


#endif //PWR_SEM3_ZMPO_LAB4_POINT_HPP
