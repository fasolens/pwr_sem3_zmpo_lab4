#include <iostream>
#include "Vehicle.hpp"
#include "Car.hpp"
#include "Boat.hpp"
#include "Amphibia.hpp"


int main() {
    Car car = Car("Opel");
    Boat yacht = Boat("Yamaha");
    Amphibia amphibia = Amphibia("War");

    int size_waypoints = 5;
    Point trip[size_waypoints];
    trip[0] = Point(12.5, 8.12);
    trip[1] = Point(2.5, 18.12);
    trip[2] = Point(22.72, 28.12);
    trip[3] = Point(13.5, 1.12);
    trip[4] = Point(0.8, 22.12);

    car.set_trip(trip, 5);
    car.go();
    std::cout << car.get_brand() << ": odometer is: " << car.get_odometer() << std::endl;

    yacht.set_trip(trip, 5);
    yacht.go();
    std::cout << yacht.get_brand() << ": odometer of yacht is: " << yacht.get_odometer() << std::endl;

    amphibia.set_trip(trip, 5);
    amphibia.go();
    std::cout << amphibia.get_brand() << ": odometer of amphibia is: " << amphibia.get_odometer() << std::endl;
    return 0;
}