//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_LAB4_AMPHIBIA_HPP
#define PWR_SEM3_ZMPO_LAB4_AMPHIBIA_HPP


#include "Car.hpp"
#include "Boat.hpp"

class Amphibia : public Car, public Boat {
protected:
public:
    Amphibia() {
        std::cout << "Amphibia default constructor" << std::endl;
    }
    Amphibia(char const* brand) : Vehicle(brand) {
        std::cout << "Amphibia brand constructor" << std::endl;
    }
    void go() {
        for (int i = 0; i < distance; ++i) {
            float wtr = trip[i].get_dst_by_wtr();
            float gnd = trip[i].get_dst_by_gnd();
            if (gnd < wtr) {
                go_by_ground(i);
            } else {
                go_by_water(i);
            }
        }
    }
};


#endif //PWR_SEM3_ZMPO_LAB4_AMPHIBIA_HPP
