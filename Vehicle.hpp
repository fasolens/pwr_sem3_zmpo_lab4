//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_LAB4_VEHICLE_HPP
#define PWR_SEM3_ZMPO_LAB4_VEHICLE_HPP


#include "Point.hpp"
#include <string>

class Vehicle {
protected:
    std::string brand;
    float odometer;
    int distance;
public:
    Point* trip;
    Vehicle() {
        this->brand = nullptr;
        odometer = 0.0;
        std::cout << "Vehicle default constructor" << std::endl;
    }
    Vehicle(char const* brand) {
        this->brand = brand;
        odometer = 0.0;
        std::cout << "Vehicle brand constructor" << std::endl;
    }
    virtual ~Vehicle() {
    }
    virtual void go() =0;
    std::string get_brand() {
        return this->brand;
    }
    void set_trip(Point t[], int distance) {
        trip = t;
        this->distance = distance;
    }
    float get_odometer() {
        return odometer;
    }
};


#endif //PWR_SEM3_ZMPO_LAB4_VEHICLE_HPP
